//
//  UserMarker+CoreDataProperties.swift
//  IRISMaps
//
//  Created by Michael on 12/5/19.
//  Copyright © 2019 Michael. All rights reserved.
//
//

import Foundation
import CoreData


extension UserMarker {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<UserMarker> {
        return NSFetchRequest<UserMarker>(entityName: "UserMarker")
    }

    @NSManaged public var loc_name: String?
    @NSManaged public var loc_description: String?
    @NSManaged public var longitude: Double
    @NSManaged public var latitude: Double
    @NSManaged public var loc_image: Data?
    @NSManaged public var thumbnail: Thumbnail?

}
