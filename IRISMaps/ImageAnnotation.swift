//
//  ImageAnnotation.swift
//  IRISMaps
//
//  Created by Michael on 12/6/19.
//  Copyright © 2019 Michael. All rights reserved.
//
import UIKit
import CoreLocation
import MapKit

class ImageAnnotation : NSObject, MKAnnotation {
    var coordinate: CLLocationCoordinate2D
    var title: String?
    var subtitle: String?
    var image: UIImage?
    var colour: UIColor?

    override init() {
        self.coordinate = CLLocationCoordinate2D()
        self.title = nil
        self.subtitle = nil
        self.image = nil
        self.colour = UIColor.white
    }
}
