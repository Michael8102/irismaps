//
//  Thumbnail+CoreDataProperties.swift
//  IRISMaps
//
//  Created by Michael on 12/5/19.
//  Copyright © 2019 Michael. All rights reserved.
//
//

import Foundation
import CoreData


extension Thumbnail {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Thumbnail> {
        return NSFetchRequest<Thumbnail>(entityName: "Thumbnail")
    }

    @NSManaged public var id: Double
    @NSManaged public var loc_image: Data?
    @NSManaged public var fullRes: UserMarker?

}
