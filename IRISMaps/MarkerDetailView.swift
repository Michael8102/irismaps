//
//  MarkerDetailView.swift
//  IRISMaps
//
//  Created by Michael on 12/6/19.
//  Copyright © 2019 Michael. All rights reserved.
//

import UIKit

@IBDesignable
class MarkerDetailView: UIView {
    
    @IBInspectable
    var cornerRadius: CGFloat {
        set { layer.cornerRadius = 20 }
        get { return layer.cornerRadius     }
    }

    @IBOutlet weak var locationImage: UIImageView!
    @IBOutlet weak var locationName: UILabel!
    @IBOutlet weak var locationDetails: UILabel!
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    var location: ImageAnnotation!
//   weak var delegate: MarkerDetailViewDelegate?
    
    override func awakeFromNib() {
       super.awakeFromNib()
        
        locationImage.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMinXMinYCorner]
        locationImage.layer.cornerRadius = 10
        locationImage.clipsToBounds = true
        
//
//       // appearance
//       backgroundContentButton.applyArrowDialogAppearanceWithOrientation(arrowOrientation: .down) // 3
     }
    
    
    func configureWithLocation(location: ImageAnnotation) { // 5
        //locationImage.layer.cornerRadius = 20.0
        locationName.textColor = UIColor.black
        locationDetails.textColor = UIColor.black
       self.location = location
    
        locationImage.image = location.image
        
        locationName.text = location.title
        locationDetails.text = location.subtitle
     }

}
