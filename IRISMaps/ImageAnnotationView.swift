//
//  ImageAnnotationView.swift
//  IRISMaps
//
//  Created by Michael on 12/6/19.
//  Copyright © 2019 Michael. All rights reserved.
//
import UIKit
import MapKit


class ImageAnnotationView: MKAnnotationView {
    private var imageView: UIImageView!
    weak var customCalloutView: UIView?

    override init(annotation: MKAnnotation?, reuseIdentifier: String?) {
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)

        self.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        self.imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        self.addSubview(self.imageView)

        self.imageView.layer.cornerRadius = 5.0
        self.imageView.layer.masksToBounds = true
    }

    override var image: UIImage? {
        get {
            return self.imageView.image
        }

        set {
            self.imageView.image = newValue
        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
//     // MARK: - callout showing and hiding
//     override func setSelected(_ selected: Bool, animated: Bool) {
//       super.setSelected(selected, animated: animated)
//    
//       if selected { // 2
//         self.customCalloutView?.removeFromSuperview() // remove old custom callout (if any)
//    
//         if let newCustomCalloutView = loadPersonDetailMapView() {
//           // fix location from top-left to its right place.
//           newCustomCalloutView.frame.origin.x -= newCustomCalloutView.frame.width / 2.0 - (self.frame.width / 2.0)
//           newCustomCalloutView.frame.origin.y -= newCustomCalloutView.frame.height
//    
//           // set custom callout view
//           self.addSubview(newCustomCalloutView)
//           self.customCalloutView = newCustomCalloutView
//    
//           // animate presentation
//           if animated {
//             self.customCalloutView!.alpha = 0.0
//             UIView.animate(withDuration: 1, animations: {
//               self.customCalloutView!.alpha = 1.0
//             })
//           }
//         }
//       } else { // 3
//         if customCalloutView != nil {
//           if animated { // fade out animation, then remove it.
//             UIView.animate(withDuration: 1, animations: {
//               self.customCalloutView!.alpha = 0.0
//             }, completion: { (success) in
//               self.customCalloutView!.removeFromSuperview()
//             })
//           } else { self.customCalloutView!.removeFromSuperview() } // just remove it.
//         }
//       }
//     }
//    
//     func loadPersonDetailMapView() -> UIView? { // 4
//       let view = UIView(frame: CGRect(x: 0, y: 0, width: 240, height: 280))
//       return view
//     }
    
    
    func loadLocationDetailMapView() -> MarkerDetailView? {
      if let views = Bundle.main.loadNibNamed("MarkerDetailMapView", owner: self, options: nil) as? [MarkerDetailView], views.count > 0 {
        let locationDetailView = views.first!
      //  locationDetailView.delegate = self.personDetailDelegate
        if let locationAnnotation = annotation as? ImageAnnotation {
            let location = locationAnnotation
          locationDetailView.configureWithLocation(location: location)
        }
        return locationDetailView
      }
      return nil
    }
}
