//
//  ViewController.swift
//  IRISMaps
//
//  Created by Michael on 12/5/19.
//  Copyright © 2019 Michael. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import CoreData


class ViewController: UIViewController, CLLocationManagerDelegate,  UIImagePickerControllerDelegate, UINavigationControllerDelegate, MKMapViewDelegate {

    @IBOutlet weak var mapView: MKMapView!
    var locationManager = CLLocationManager()
    
    //MARK: Other Variables
    //Other variables
    var image: UIImage!
    var prevImage:UIImage!
    var locationName: String = ""
    var locationDescription: String = ""
    var calloutView: MarkerDetailView!
    var totalMarkers: Int = 0
    var nearbyImages = [UIImage]()
    var nearbyNames = [String?]()
    
    //MARK: Dropdown Properties
    
    var transparentView = UIView()
    var tableView = UITableView()
       
    let height: CGFloat = 250
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.isScrollEnabled = true
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(CustomTableViewCell.self, forCellReuseIdentifier: "Cell")
        // Do any additional setup after loading the view.
        mapView.delegate = self
        
        mapView.showsUserLocation = true
        mapView.mapType = .hybrid

        let locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        // Check for Location Services
        if (CLLocationManager.locationServicesEnabled()) {
            locationManager.requestAlwaysAuthorization()
            locationManager.requestWhenInUseAuthorization()
        }
        //Zoom to user location
        if let userLocation = self.locationManager.location?.coordinate {
            let viewRegion = MKCoordinateRegion(center: userLocation, latitudinalMeters: 200, longitudinalMeters: 200)
            mapView.setRegion(viewRegion, animated: false)
        }
        self.locationManager = locationManager
        DispatchQueue.main.async {
            self.locationManager.startUpdatingLocation()
        }
        loadMap()
        
//        self.doLayout()
    }

    
    // MARK: Taking Image
    
    @IBAction func addNewLocation(_ sender: Any) {
        
        let imagePicker = UIImagePickerController()
          imagePicker.delegate = self
          imagePicker.sourceType = .camera
          imagePicker.allowsEditing = false
          self.present(imagePicker, animated: true, completion: nil)
 
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
      //Dismiss Camera
      picker.dismiss(animated: true, completion: nil)
      
      //Retrieve Picture
        image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        nearbyImages.append(image)
        nearbyNames.append(locationName)
       showInputDialog()
        
      
      //CONTINUE HERE
      
    }
            func showInputDialog(){
                  //Creating UIAlertController and
                    //Setting title and message for the alert dialog
                    let alertController = UIAlertController(title: "Location details", message: "Enter location name and description", preferredStyle: .alert)
                    //the confirm action taking the inputs
                    let confirmAction = UIAlertAction(title: "Save", style: .default) { (_) in
                        //getting the input values from user
                        self.locationName = (alertController.textFields?[0].text)!
                        self.locationDescription = (alertController.textFields?[1].text)!
                        self.dropPins(latitude: self.locationManager.location!.coordinate.latitude, longitude: self.locationManager.location!.coordinate.longitude, image: self.image)
                            
    //                    self.saveData()
                        }
                    //the cancel action doing nothing
                    let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (_) in
        
    //                    self.sceneView.session.remove(anchor: anchor)
        
                }
                    //adding textfields to our dialog box
                    alertController.addTextField { (textField) in
                        textField.placeholder = "Enter Location Name"
                    }
                    alertController.addTextField { (textField) in
                        textField.placeholder = "Enter Location Description"
                    }
                    //adding the action to dialogbox
                    alertController.addAction(confirmAction)
                    alertController.addAction(cancelAction)
        
                    //finally presenting the dialog box
                    self.present(alertController, animated: true, completion: nil)
                }
    
    //MARK: Placing Markers
    
    func dropPins(latitude: Double, longitude: CLLocationDegrees, image: UIImage){
        let annotation = ImageAnnotation()  // <-- new instance here
        annotation.coordinate.longitude = longitude
        annotation.coordinate.latitude = latitude
        annotation.title = locationName
        annotation.subtitle = locationDescription
        annotation.image = image
        self.mapView.addAnnotation(annotation)
        if let imageData = image.jpegData(compressionQuality: 1.0){
                   saveData(imageData: imageData, annotation: annotation)
               }
        
    }
    
    
    func saveData(imageData: Data, annotation: ImageAnnotation){
                      do {
                            guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {return}
                            let managedContext = appDelegate.persistentContainer.viewContext
                            let newMarker = NSEntityDescription.entity(forEntityName: "UserMarker", in: managedContext)
                            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "UserMarker")
                        //request.predicate = NSPredicate(format: "longitude = %@", annotation.coordinate.longitude)
                            do
                                   {
                                    let useLat: String = "\(annotation.coordinate.latitude)"
                                    let useLong: String = "\(annotation.coordinate.longitude)"
                                    
                                        let location = NSManagedObject(entity: newMarker!, insertInto: managedContext)
                                          location.setValue(imageData, forKey: "loc_image")
                                        location.setValue(useLat, forKey: "latitude")
                                        location.setValue(useLong, forKey: "longitude")
                                        location.setValue(self.locationName, forKey: "loc_name")
                                        location.setValue(self.locationDescription, forKey: "loc_description")
    
                                         do{
                                            try managedContext.save()
                                          print("save successful")
                                            //self.showToast(message: "Save Successful")
                                          
                                         }catch let error as NSError{
                                             print("could not save\(error)")
                                        }
                                    //}
                                   } catch {
                                     print("error executing fetch request: \(error)")
                                  }
                          }
                      }
    
    func loadMap(){
                 guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {return}
                 let managedContext = appDelegate.persistentContainer.viewContext
     
                 let request = NSFetchRequest<NSFetchRequestResult>(entityName: "UserMarker")
 
                 do
                 {
                     let results = try managedContext.fetch(request)
                     for all in results{
                        totalMarkers = results.count
                     }
                        if results.count >= 1
                     {
                        for marker in results{
                           let  currentMarker = marker as! NSManagedObject
                            
                            let loadedImage = currentMarker.value(forKey: "loc_image")
                            prevImage = UIImage(data: loadedImage as! Data)
                        //    let deg = currentMarker.value(forKey: "latitude")
                           
                            
                            if let lat = currentMarker.value(forKey: "latitude") as? String, let doubLat = Double(lat) {
                               if let longi = currentMarker.value(forKey: "longitude") as? String, let doubLong = Double(longi){
                                
                                nearbyImages.append(prevImage)
                                nearbyNames.append((currentMarker.value(forKey: "loc_name") as? String))
                                let annotation = ImageAnnotation()  // <-- new instance here
                                annotation.coordinate.longitude = doubLong
                                annotation.coordinate.latitude = doubLat
                                annotation.title = currentMarker.value(forKey: "loc_name") as? String
                                annotation.subtitle = currentMarker.value(forKey: "loc_description") as? String
                                annotation.image = prevImage
                                self.mapView.addAnnotation(annotation)
                                
                                //dropPins(latitude: doubLat, longitude: doubLong, image: prevImage)
                           }
                                
                        }
//                         showToast(message: "found location")
//
                        }
                        
                        //nodeName = foundUser.value(forKey: "loc_name")
                        //print(loadedImage)
     
                     }else if  results.count == 0{
                        print("Not the same location")
                         //showToast(message: "Found New Location")
     
                  }
     
                 } catch {
                     print("error executing fetch request: \(error)")
                  }
     
             }
    
//    function measure(lat1: Double, lon1: Double, lat2: Double, lon2: Double){  // generally used geo measurement function
//        var R = 6378.137; // Radius of earth in KM
//        var dLat = lat2 * Double.pi / 180 - lat1 *  Double.pi / 180;
//        var dLon = lon2 *  Double.pi / 180 - lon1 *  Double.pi / 180;
//        var a = sin(dLat/2) * sin(dLat/2) +
//        cos(lat1 *  Double.pi / 180) * cos(lat2 *  Double.pi / 180) *
//        sin(dLon/2) * sin(dLon/2);
//        var c = 2 * tan(sqrt(a), sqrt(1-a));
//        var d = R * c;1
//        return d * 1000; // meters
//    }
  
    

    
    @IBAction func popUpView(_ sender: Any) {
        
        let window = UIApplication.shared.keyWindow
        transparentView.backgroundColor = UIColor.black.withAlphaComponent(0.9)
        transparentView.frame = self.view.frame
        window?.addSubview(transparentView)
        
        let screenSize = UIScreen.main.bounds.size
        tableView.frame = CGRect(x: 0, y: screenSize.height, width: screenSize.width, height: height)
        window?.addSubview(tableView)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(onClickTransparentView))
        transparentView.addGestureRecognizer(tapGesture)
        
        transparentView.alpha = 0
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1.0, initialSpringVelocity: 1.0, options: .curveEaseInOut, animations: {
            self.transparentView.alpha = 0.5
            self.tableView.frame = CGRect(x: 0, y: screenSize.height - self.height, width: screenSize.width, height: self.height)
        }, completion: nil)
        
    }
    
}

extension ViewController{
    
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation.isKind(of: MKUserLocation.self) {  //Handle user location annotation..
            return nil  //Default is to let the system handle it.
        }

        if !annotation.isKind(of: ImageAnnotation.self) {  //Handle non-ImageAnnotations..
            var pinAnnotationView = mapView.dequeueReusableAnnotationView(withIdentifier: "DefaultPinView")
            if pinAnnotationView == nil {
                pinAnnotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "DefaultPinView")
            }
            return pinAnnotationView
        }

        //Handle ImageAnnotations..
        var view: ImageAnnotationView? = mapView.dequeueReusableAnnotationView(withIdentifier: "imageAnnotation") as? ImageAnnotationView
        if view == nil {
            view = ImageAnnotationView(annotation: annotation, reuseIdentifier: "imageAnnotation")
        }

        let annotation = annotation as! ImageAnnotation
        //annotation.title
        view?.image = annotation.image
        view?.annotation = annotation

        return view
    }

     func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView){
        // 1
        if view.annotation is MKUserLocation{
           // Don't proceed with custom callout
           return
        }
        
          // 2
          let locationAnnotation = view.annotation as! ImageAnnotation
          let views = Bundle.main.loadNibNamed("MarkerDetailMapView", owner: nil, options: nil)
           calloutView = views?[0] as! MarkerDetailView
        DispatchQueue.main.async {
            //calloutView.configureWithLocation(location: locationAnnotation)
            self.calloutView.locationName.text = locationAnnotation.title
            
            self.calloutView.locationDetails.text = locationAnnotation.subtitle
        }
        
        calloutView.locationImage.image = locationAnnotation.image

          calloutView.center = CGPoint(x: view.bounds.size.width / 50, y: -calloutView.bounds.size.height*0.052)
        view.addSubview(calloutView)
          mapView.setCenter((view.annotation?.coordinate)!, animated: true)
    
        print(mapView.selectedAnnotations.count)
    }
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        
            //mapView.deselectAnnotation(view.annotation, animated: true)
        if view.annotation is MKUserLocation{
            return
        }else{
            calloutView.removeFromSuperview()
        }
        
    
        
        
        print("test")
    }
    
    
}


//MARK: Slide Up View

extension ViewController{
    
 
        
        //var settingArray = ["Profile","Favorite","Notification","Change Password","Logout"]


        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }

        @IBAction func onClickMenu(_ sender: Any) {
            
            let window = UIApplication.shared.keyWindow
            transparentView.backgroundColor = UIColor.white.withAlphaComponent(0.9)
            transparentView.frame = self.view.frame
            window?.addSubview(transparentView)
            
            let screenSize = UIScreen.main.bounds.size
            tableView.frame = CGRect(x: 0, y: screenSize.height, width: screenSize.width, height: height)
            window?.addSubview(tableView)
            
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(onClickTransparentView))
            transparentView.addGestureRecognizer(tapGesture)
            
            transparentView.alpha = 0
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1.0, initialSpringVelocity: 1.0, options: .curveEaseInOut, animations: {
                self.transparentView.alpha = 0.5
                self.tableView.frame = CGRect(x: 0, y: screenSize.height - self.height, width: screenSize.width, height: self.height)
            }, completion: nil)
            
        }
        
        @objc func onClickTransparentView() {
            let screenSize = UIScreen.main.bounds.size

            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1.0, initialSpringVelocity: 1.0, options: .curveEaseInOut, animations: {
                self.transparentView.alpha = 0
                self.tableView.frame = CGRect(x: 0, y: screenSize.height, width: screenSize.width, height: self.height)
            }, completion: nil)
        }
        
    }

    extension ViewController: UITableViewDataSource, UITableViewDelegate {
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            print(totalMarkers)
            return totalMarkers
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as? CustomTableViewCell else {fatalError("Unable to deque cell")}
            cell.lbl.text = nearbyNames[indexPath.row]
            cell.settingImage.image = nearbyImages[indexPath.row]
            return cell
        }
        
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 80
        }
        
        
    
}
